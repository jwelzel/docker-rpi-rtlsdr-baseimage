FROM resin/rpi-raspbian:stretch

MAINTAINER Jochen Welzel

RUN apt-get update && apt-get upgrade && \
    apt-get install -y --no-install-recommends \
    build-essential \
    ca-certificates \
    cmake \
    git \
    git-core \
    libusb-1.0-0 \
    libusb-1.0-0-dev \
    pkg-config \
    wget

RUN echo 'blacklist dvb_usb_rtl28xxu' > /etc/modprobe.d/rtlsdr-blacklist.conf

WORKDIR /tmp

RUN git clone git://git.osmocom.org/rtl-sdr.git \
    && cd rtl-sdr \
    && mkdir build

WORKDIR /tmp/rtl-sdr/build
RUN cmake ../ -DINSTALL_UDEV_RULES=ON -DDETACH_KERNEL_DRIVER=ON \
    && make \
    && make install \
    && ldconfig

# Cleanup
RUN apt-get remove -y build-essential \
    ca-certificates \
    cmake \
    git \
    git-core \
    libusb-1.0-0-dev \
    pkg-config \
    && apt-get autoremove \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

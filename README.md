# Docker RTLSDR Baseimage based on Debian Stretch for Raspberry Pi


### Thanks
* To Frederik Granna for his [blog post](http://www.sysrun.io/2015/11/20/a-complete-docker-rpi-rtl-sdr-adsbacars-solution/) and some docker/code inspirations
* To Glenn Stewart for his [piaware containers](https://hub.docker.com/u/inodes/) 
